import { Link } from "react-router-dom";
import { Outlet } from "react-router-dom";
import { useEffect } from "react";
import basketIcon from "../components/assets/shopping-basket.svg";
import basicFavorite from "../components/assets/favorite-false.svg";
import "./Layout.scss";
import "./reset.scss";
import { useSelector, useDispatch } from "react-redux";
import { basketReducer, favoriteReducer } from "../productsSlice";

function Layout() {
  const basketProducts = useSelector((state) => state.products.basket);
  const favProducts = useSelector((state) => state.products.favorite);
  const dispatch = useDispatch();

  const getDataFromLS = (key) => {
    const lsData = localStorage.getItem(key);
    if (!lsData) return [];
    try {
      const value = JSON.parse(lsData);
      return value;
    } catch (e) {
      return [];
    }
  };

  useEffect(() => {
    dispatch(basketReducer(getDataFromLS("Basket")));
    dispatch(favoriteReducer(getDataFromLS("Favorite")));
  }, []);

  useEffect(() => {
    localStorage.setItem("Basket", JSON.stringify(basketProducts));
    localStorage.setItem("Favorite", JSON.stringify(favProducts));
  }, [basketProducts, favProducts]);
  return (
    <>
      <header className="header">
        <h1>Header Logo</h1>
        <div className="header__div">
          <img src={basketIcon} className="header__photo" alt="basket" />
          <p className="header__text">{basketProducts.length}</p>
          <img src={basicFavorite} className="header__photo" alt="favorite" />
          <p className="header__text">{favProducts.length}</p>
        </div>
      </header>
      <nav className="nav">
        <Link to="/" className="nav__link">
          Main Page
        </Link>
        <Link to="/basket" className="nav__link">
          Basket
        </Link>
        <Link to="/favorite" className="nav__link">
          Favorite
        </Link>
      </nav>

      <Outlet />
      <footer className="footer">
        <h3>Footer info</h3>
      </footer>
    </>
  );
}

export default Layout;
