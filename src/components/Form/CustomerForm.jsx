import { Form, Field, ErrorMessage, Formik } from "formik";
import "./CustomerForm.css";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { basketReducer } from "../../productsSlice";
import "yup-phone";

const SignScheme = Yup.object().shape({
  name: Yup.string()
    .required("Name is required!")
    .min(5, "Name is too short!")
    .max(10, "Name is too long!"),
  surname: Yup.string()
    .required("Surname is required!")
    .min(5, "Surname is too short!")
    .max(15, "Surname is too long!"),
  dateBirth: Yup.date()
    .max(new Date(), "Date of birth must be in the past!")
    .required("Date of birth is required!"),
  adress: Yup.string().required("Adress is required!"),
  phoneNumber: Yup.string()
    .matches(/^\+?[0-9]+\d{9}$/, {
      message: "Please enter valid number.",
      excludeEmptyString: false,
    })
    .required("Phone number is required!"),
});

const initialValues = {
  name: "",
  surname: "",
  dateBirth: "",
  adress: "",
  phoneNumber: "",
};

function SignInForm() {
  const dispatch = useDispatch();
  const basket = useSelector((state) => state.products.basket);

  return (
    <>
      {basket.length > 0 ? (
        <Formik
          initialValues={initialValues}
          validationSchema={SignScheme}
          onSubmit={(values, { resetForm }) => {
            if (basket.length >= 1) {
              console.log("Customer items:", basket);
              console.log("Customer info:", values);
              dispatch(basketReducer([]));
            }
            resetForm();
          }}
        >
          {(formik) => {
            const { errors, touched, isValid, dirty } = formik;
            return (
              <div className="form">
                <h1>Enter information to complete order</h1>
                <Form className="pageForm">
                  <div className="pageDiv">
                    <label htmlFor="name">Your name:</label>
                    <Field
                      type="text"
                      name="name"
                      id="name"
                      placeholder="Please, enter name:"
                      className={
                        errors.name && touched.name
                          ? "input-error"
                          : touched.name
                          ? "correct-input"
                          : "usual-input"
                      }
                    />

                    <ErrorMessage
                      name="name"
                      component="span"
                      className="error"
                    />
                  </div>
                  <div className="pageDiv">
                    <label htmlFor="surname">Your surname:</label>
                    <Field
                      type="text"
                      name="surname"
                      id="surname"
                      placeholder="Please, enter surname:"
                      className={
                        errors.surname && touched.surname
                          ? "input-error"
                          : touched.surname
                          ? "correct-input"
                          : "usual-input"
                      }
                    />
                    <ErrorMessage
                      name="surname"
                      component="span"
                      className="error"
                    />
                  </div>
                  <div className="pageDiv">
                    <label htmlFor="dateBirth">Your date of birth:</label>
                    <Field
                      type="date"
                      name="dateBirth"
                      id="dateBirth"
                      placeholder="Please, enter your date og birth:"
                      className={
                        errors.dateBirth && touched.dateBirth
                          ? "input-error"
                          : touched.dateBirth
                          ? "correct-input"
                          : "usual-input"
                      }
                    />
                    <ErrorMessage
                      name="dateBirth"
                      component="span"
                      className="error"
                    />
                  </div>
                  <div className="pageDiv">
                    <label htmlFor="adress">Your adress:</label>
                    <Field
                      type="text"
                      name="adress"
                      id="adress"
                      placeholder="Please, enter adress:"
                      className={
                        errors.adress && touched.adress
                          ? "input-error"
                          : touched.adress
                          ? "correct-input"
                          : "usual-input"
                      }
                    />
                    <ErrorMessage
                      name="adress"
                      component="span"
                      className="error"
                    />
                  </div>
                  <div className="pageDiv">
                    <label htmlFor="phoneNumber">Your phone number:</label>
                    <Field
                      type="text"
                      name="phoneNumber"
                      id="phoneNumber"
                      placeholder="Please, enter phoneNumber:"
                      className={
                        errors.phoneNumber && touched.phoneNumber
                          ? "input-error"
                          : touched.phoneNumber
                          ? "correct-input"
                          : "usual-input"
                      }
                    />
                    <ErrorMessage
                      name="phoneNumber"
                      component="span"
                      className="error"
                    />
                  </div>
                  <button
                    type="submit"
                    className={
                      !(dirty && isValid) ? "disabled-btn" : "active-btn"
                    }
                    disabled={!(dirty && isValid)}
                  >
                    Checkout
                  </button>
                </Form>
              </div>
            );
          }}
        </Formik>
      ) : null}
    </>
  );
}

export default SignInForm;
