import "./Modal.scss";

const ModalFooter = ({
  firstText,
  secondaryText,
  firstClick,
  secondaryClick,
}) => {
  return (
    <>
      {firstClick && firstText && !secondaryClick && !secondaryText ? (
        <div className="modal__footer">
          <button
            type="submit"
            className="new__button new__button--purple"
            onClick={firstClick}
          >
            {firstText}
          </button>
        </div>
      ) : null}
      {firstClick && firstText && secondaryClick && secondaryText ? (
        <div className="modal__footer">
          <button
            type="submit"
            className="new__button new__button--purple"
            onClick={firstClick}
          >
            {firstText}
          </button>
          <button
            type="submit"
            className="new__button new__button--white"
            onClick={secondaryClick}
          >
            {secondaryText}
          </button>
        </div>
      ) : null}
    </>
  );
};

export default ModalFooter;
