function ModalImage({ image, className }) {
  return <img src={image} className={className}></img>;
}

export default ModalImage;
