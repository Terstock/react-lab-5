import Button from "../Button/Button";
import "../Card/Card.scss";
import "../Button/Button.scss";
import "../Modal/Modal.scss";
import Modal from "../Modal/Modal";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalHeader from "../Modal/ModalHeader";
import ModalClose from "../Modal/ModalClose";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";
import ModalImage from "../Modal/ModalImage";
import ModalText from "../Modal/ModalText";
import falseFavorite from "../assets/favorite-false.svg";
import trueFavorite from "../assets/favorite-true.svg";
import { useEffect } from "react";
import PropTypes from "prop-types";
import "./Card.scss";
import { useDispatch, useSelector } from "react-redux";
import { modalReducer, setModalType, setIsVisible } from "../../modalSlice";
import {
  addProduct,
  deleteFromBasket,
  handleFavorite,
} from "../../productsSlice";

function Card({ card, pageType }) {
  const dispatch = useDispatch();
  const isVisible = useSelector((state) => state.modal.isVisible);
  const modalType = useSelector((state) => state.modal.modalType);
  const basketProducts = useSelector((state) => state.products.basket);
  const favProducts = useSelector((state) => state.products.favorite);

  const isFavoritesAlready = favProducts.some(
    (product) => product.id === card.id
  );
  const isInBasket = basketProducts.some((product) => product.id === card.id);

  function openModal(type) {
    dispatch(setModalType(type));
    dispatch(setIsVisible(card.id));
    dispatch(modalReducer("true"));
  }

  function closeModal() {
    dispatch(setModalType("unknown"));
    dispatch(setIsVisible(null));
    dispatch(modalReducer("false"));
  }

  function handleWrapperClick(e) {
    e.stopPropagation();
  }

  //Scroll remove
  useEffect(() => {
    if (isVisible === card.id) {
      document.body.classList.add("no-scroll");
    } else {
      document.body.classList.remove("no-scroll");
    }
  }, [isVisible, card.id]);

  return (
    <>
      <li className="card">
        {pageType === "presentIcon" ? (
          <button
            className="delete-btn"
            title="Delete Product"
            onClick={() => openModal("delete")}
          >
            ✖
          </button>
        ) : null}

        <img className="card__photo" src={card.path} alt="" />
        <button
          className="card__button"
          onClick={() => {
            dispatch(handleFavorite(card));
          }}
        >
          <img
            className="card__button-img"
            src={isFavoritesAlready ? trueFavorite : falseFavorite}
            alt=""
          />
        </button>
        <div className="card__info">
          <h1 className="card__title">{card.name}</h1>
          <h2 className="card__price">${card.price}</h2>
          <h3 className="card__color">Game disk color: {card.color}</h3>
          <h3 className="card__vendor-code">№{card.id}</h3>

          <div className="app-div">
            <Button
              type="button"
              onClick={() => openModal("add")}
              className="new__button new__button--white"
              disabled={isInBasket}
            >
              {isInBasket ? "Product is in Basket" : "Buy Product"}
            </Button>
          </div>
        </div>
      </li>
      {isVisible === card.id ? (
        <ModalWrapper className="modal__wrapper" onClick={closeModal}>
          <Modal className="modal" onClick={handleWrapperClick}>
            <ModalHeader className="modal__header">
              <ModalClose
                className="modal__close-btn"
                onClick={closeModal}
              ></ModalClose>
            </ModalHeader>
            <ModalBody className="modal__body">
              {modalType === "delete" ? (
                <>
                  <ModalImage
                    className="modal__body-rectangle"
                    image={card.path}
                  />
                  <ModalText className="modal__body-title">
                    Product Delete!
                  </ModalText>
                  <ModalText className="modal__body-text">
                    By clicking the “Yes, Delete” button, {card.name} will be
                    deleted.
                  </ModalText>
                </>
              ) : (
                <>
                  <ModalText className="modal__body-title">
                    Add Product {card.name}
                  </ModalText>
                  <ModalText className="modal__body-text">
                    Description for {card.name}
                  </ModalText>
                </>
              )}
            </ModalBody>
            {modalType === "delete" ? (
              <ModalFooter
                firstText="NO, CANCEL"
                secondaryText="YES, DELETE"
                firstClick={closeModal}
                secondaryClick={() => {
                  dispatch(deleteFromBasket(card)), closeModal();
                }}
              ></ModalFooter>
            ) : (
              <ModalFooter
                firstText="ADD TO BASKET"
                firstClick={() => {
                  dispatch(addProduct(card)), closeModal();
                }}
              ></ModalFooter>
            )}
          </Modal>
        </ModalWrapper>
      ) : null}
    </>
  );
}

Card.propTypes = {
  card: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    path: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
  }).isRequired,
};

export default Card;
