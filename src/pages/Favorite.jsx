import List from "../components/List/List";
import { useSelector } from "react-redux";

function Favorite() {
  const favProducts = useSelector((state) => state.products.favorite);
  return (
    <>
      <List data={favProducts} />
    </>
  );
}

export default Favorite;
