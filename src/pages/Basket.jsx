import List from "../components/List/List";
import CustomerForm from "../components/Form/CustomerForm";
import { useSelector } from "react-redux";

function Basket() {
  const basketProducts = useSelector((state) => state.products.basket);
  let pageType = "presentIcon";
  return (
    <div className="basket-div">
      <CustomerForm />
      <List data={basketProducts} pageType={pageType} />
    </div>
  );
}

export default Basket;
