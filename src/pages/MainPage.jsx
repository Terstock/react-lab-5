import { useEffect } from "react";
import List from "../components/List/List";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts, setError } from "../productsSlice";

function MainPage() {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.products);
  const productStatus = useSelector((state) => state.products.status);
  const error = useSelector((state) => state.products.error);

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  let content;

  if (productStatus === "loading") {
    content = <div>Loading...</div>;
  } else if (productStatus === "succeeded") {
    content = <List data={products} />;
  } else if (productStatus === "failed") {
    dispatch(setError("The path is incorrect!"));
    content = <div>{error}</div>;
  }

  return <>{content}</>;
}

export default MainPage;
